<?php
/**
 * Gestion du formulaire de configuration du layout du plugin.
 *
 * @package SPIP\ZGALA\ADMINISTRATION
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement des saisies :
 *
 * @return array Tableau des valeurs.
 */
function formulaires_configurer_zgala_layout_saisies() {
	// Extraction des données relatives au layout
	// - données de configuration du layout
	include_spip('inc/config');
	$configuration = lire_config('zgala_config/layout', []);
	// - valeurs par défaut des paramètres du layout
	$defauts = lire_config('zgala_defaut/layout', []);
	// - valeurs courantes des paramètres du layout
	$parametres = lire_config('zgala/layout', []);

	// Saisie hidden pour définir le casier
	$saisie_casier = [
		'saisie'  => 'hidden',
		'options' => [
			'nom'    => '_meta_casier',
			'defaut' => 'zgala/layout',
		],
	];

	// Saisies du fieldset d'agencement permettant de choisir l'id du layout
	// ---------------------------------------------------------------------
	// Aucune de ces saisies ne sont incluses dans les paramètres finaux, elles ne sont qu'intermédiaires pour choisir
	// In fine l'id du layout.Les saisies intermédiaires portent un nom qui commence par un _
	//

	// - Valeurs par défaut : on écrase les valeurs par défaut de l'agencement définies par le plugin par celles
	//   issues du layout courant, si il existe.
	if (!empty($parametres['id'])) {
		// il existe déjà un layout choisi : on extrait les défaut de ce layout
		$defauts['_columns'] =  (int) substr($configuration['structures'][$parametres['id']]['group'], 0, 1);
		$defauts['_group_' . $defauts['_columns']] = $configuration['structures'][$parametres['id']]['group'];
	}

	// - Fieldset
	$saisies_agencement = [
		'saisie'  => 'fieldset',
		'options' => [
			'nom'   => 'agencement',
			'label' => '<:zgala_config:legende_layout_agencement:>',
			'onglet' => true,
			'onglet_horizontal' => true,
		],
	];

	// - Nombre de colonnes
	// -- Labels
	$columns = [2, 3];
	$data = [];
	foreach ($columns as $_nb) {
		$data[$_nb] = _T("zgala_config:saisie_layout_columns_{$_nb}_option");
	}
	$saisies_agencement['saisies'][] = [
		'saisie'  => 'radio',
		'options' => [
			'nom'         => '_columns',
			'label'       => '<:zgala_config:saisie_layout_columns_label:>',
			'data'        => $data,
			'obligatoire' => true,
			'defaut'      => $defauts['_columns'],
			'conteneur_class' => 'pleine_largeur',
		],
	];

	// - Groupes de layouts pour le nombre de colonnes sélectionné
	include_spip('config/zgala');
	$groupes = [];
	foreach ($columns as $_nb) {
		// -- lister les ids de groupe (pour le nombre de colonnes)
		$groupes[$_nb] = zgala_configuration_layout_groupes($_nb);
		// -- construire le tableau des data de la saisie
		$data = [];
		foreach ($groupes[$_nb] as $_id_groupe) {
			$data[$_id_groupe] = _T("zgala_config:saisie_layout_group_{$_id_groupe}_option");
		}
		// -- Insérer la saisie
		$saisies_agencement['saisies'][] = [
			'saisie' => 'radio',
			'options' => [
				'nom' => "_group_{$_nb}",
				'label' => '<:zgala_config:saisie_layout_group_label:>',
				'data' => $data,
				'obligatoire' => true,
				'defaut' => $defauts["_group_{$_nb}"],
				'conteneur_class' => 'pleine_largeur',
				'afficher_si' => "@_columns@ == {$_nb}"
			],
		];
	}

	// - Choix du layout du groupe sélectionné
	// -- construction de la liste des layouts par groupe
	$balise_img = charger_filtre('balise_img');
	$layouts = [];
	foreach ($configuration['structures'] as $_id_layout => $_layout) {
		$fichier = 'layout' . str_pad("{$_id_layout}", 3, "0", STR_PAD_LEFT) . '.gif';
		$chemin = chemin_image("layouts/{$fichier}");
		$layouts[$_layout['group']][$_id_layout] = $balise_img($chemin, "Layout {$_id_layout}");
	}
	// -- insérer les layouts de chaque groupe dans une saisie
	foreach ($columns as $_nb) {
		foreach ($groupes[$_nb] as $_id_groupe) {
			$saisie_layout = [
				'saisie' => 'radio',
				'options' => [
					'nom' => "_id_{$_id_groupe}",
					'label' => '<:zgala_config:saisie_layout_id_label:>',
					'data' => $layouts[$_id_groupe],
					'obligatoire' => true,
					'conteneur_class' => 'grid grid-cols-3 gap-5 pleine_largeur',
					'afficher_si' => "@_columns@ == {$_nb} && @_group_{$_nb}@ == '{$_id_groupe}'"
				],
			];
			// -- insérer la valeur par défaut correspondant au layout courant si il existe
			if (
				!empty($parametres['id'])
				&& ($_id_groupe === $defauts["_group_{$_nb}"])
			) {
				$saisie_layout['options']['defaut'] = $parametres['id'];
			}
			$saisies_agencement['saisies'][] = $saisie_layout;
		}
	}
	// -- insérer un hidden de l'id du layout pour que les fonctions CVT retrouvent la bonne liste de saisies
	//    à sauvegarder
	$saisies_agencement['saisies'][] = [
		'saisie'  => 'hidden',
		'options' => [
			'nom'    => 'id',
			'defaut' => $defauts['id'],
		],
	];

	// Saisies du fieldset de dimensionnement de la page et des blocs
	// --------------------------------------------------------------

	// - Fieldset
	$saisies_dimensionnement = [
		'saisie'  => 'fieldset',
		'options' => [
			'nom'   => 'dimensionnement',
			'label' => '<:zgala_config:legende_layout_dimensionnement:>',
			'onglet' => true,
			'onglet_horizontal' => true,
		],
	];

	// - Tailles des blocs
	// -- de la page si fixe uniquement (en pixels)
	// -- du bloc `wrapper` fluide (en fraction)
	// -- du bloc `aside` fluide (en fraction) ou fixe (en pixels)
	// -- du bloc `extra` fluide (en fraction) ou fixe (en pixels)
	foreach ($configuration['sizes'] as $_parametre => $_groupes) {
		foreach ($columns as $_nb) {
			$groupes_autorises[$_nb] = implode(',', $_groupes[$_nb]);
		}
		$saisies_dimensionnement['saisies'][] = [
			'saisie'  => 'input',
			'options' => [
				'nom'         => $_parametre,
				'label'       => "<:zgala_config:saisie_layout_{$_parametre}_label:>",
				'defaut'      => $defauts[$_parametre],
				'afficher_si' => "(@_columns@ == 2 && @_group_2@ IN '{$groupes_autorises[2]}')
								|| (@_columns@ == 3 && @_group_3@ IN '{$groupes_autorises[3]}')"
			],
		];
	}

	return [
		$saisie_casier,
		$saisies_agencement,
		$saisies_dimensionnement,
	];
}

/**
 * Traitement des saisies :
 *
 * @return array Tableau des messages de retour.
 */
function formulaires_configurer_zgala_layout_traiter() {
	// Récupération des saisies temporaires permettant de définir le layout
	$nb_colonnes = (int) _request('_columns');
	$groupe = _request("_group_{$nb_colonnes}");
	$id_layout = (int) _request("_id_{$groupe}");
	set_request('id', $id_layout);

	// Récupération des dimensions utiles au layout choisi et suppression des autres de façon
	// à ne sauvegarder que le nécessaire.
	// - valeurs par défaut des paramètres du layout
	$defauts = lire_config('zgala_defaut/layout', []);

	// - traitement des paramètres utiles
	include_spip('config/zgala');
	$parametres_utiles = zgala_configuration_layout_parametres($groupe, $nb_colonnes);
	foreach ($parametres_utiles as $_parametre => $_est_utile) {
		if (!$_est_utile) {
			set_request($_parametre, null);
		} elseif (!_request($_parametre)) {
			set_request($_parametre, $defauts[$_parametre]);
		}
	}

	// On enregistre le formulaire en utilisant le fonctionnement CVT de base
	include_spip('inc/cvt_configurer');
	$trace = cvtconf_formulaires_configurer_enregistre('configurer_zgala_layout', []);

	return [
		'message_ok' => _T('config_info_enregistree') . $trace,
		'editable' => true
	];
}
