<?php
/**
 * Ce fichier contient l'action `zgala_configuration_recharger` lancée par un webmestre pour
 * recharger la configuration statique et les valeurs par défaut des paramètres du squelette.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à un webmestre de recharger la configuration statique et les valeurs par défaut des
 * paramètres du squelette.
 *
 * Cette action est réservée aux webmestres.
 *
 * @return void
 */
function action_zgala_configuration_recharger_dist() : void {
	// Sécurisation.
	// -- Aucun argument attendu.

	// Verification des autorisations : pour recharger laconfiguration il suffit
	// d'avoir l'autorisation "configurer".
	if (!autoriser('webmestre')) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// Rechargement
	include_spip('config/zgala');
	// -- Configuration statique
	zgala_configuration_ecrire();
	// -- Valeurs par défaut des paramètres
	zgala_defaut_ecrire();
}
