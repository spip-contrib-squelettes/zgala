<?php
return [
	// B
	'bouton_recharger' => 'Recharger la configuration',

	// C
	'casier_accueil_description' => 'à compléter pour accueil',
	'casier_accueil_menu'        => 'Accueil',
	'casier_layout_description'  => 'Le squelette Z-Gala est basé sur les <a href="https://www.html.it/app/uploads/blog/layoutgala/">Layouts Gala</a> d’Alessandro Fulciniti qui permettent de jouer sur la position des colonnes. Cette page vous permet de choisir le layout qui vous convient et de paramétrer ses dimensions.',
	'casier_layout_menu'         => 'Layout',
	'casier_layout_titre'        => 'Paramètres du layout',

	// P
	'page_configuration_titre' => 'Paramétrage du plugin',
	'page_configuration_menu'  => 'Configurer Z-Gala',

	// L
	'legende_layout_agencement'      => 'Agencement',
	'legende_layout_dimensionnement' => 'Dimensionnement',

	// S
	'saisie_layout_id_erreur'                  => 'Le numéro d\'un layout Gala est toujours compris entre 1 et 40.',
	'saisie_layout_id_explication'             => 'Le squelette Z-Gala permet de choisir le layout Gala à utiliser par défaut sur l\'ensemble du site. Saisissez un numéro entre 1 et 40 pour indiquer le layout de votre choix.',
	'saisie_layout_id_label'                   => 'Layout Gala à utiliser',
	'saisie_layout_columns_label'              => 'Nombre de colonnes du layout',
	'saisie_layout_columns_2_option'           => '2 colonnes',
	'saisie_layout_columns_3_option'           => '3 colonnes',
	'saisie_layout_group_label'                => 'Groupe de layouts',
	'saisie_layout_group_explication'          => 'Au sein d\'un groupe, le caractère fluide ou fixe des blocs est identique pour tous les layouts.',
	'saisie_layout_group_3lll_option'          => 'page fluide, `navigation` et `extra` fluides',
	'saisie_layout_group_3fff_option'          => 'page fixe, `navigation` et `extra` fixes',
	'saisie_layout_group_3lff_option'          => 'page fluide, `navigation` et `extra` fixes',
	'saisie_layout_group_3lfl_option'          => 'page fluide, `navigation` fixe et `extra` fluide',
	'saisie_layout_group_3llf_option'          => 'page fluide, `navigation` fluide et `extra` fixe',
	'saisie_layout_group_2lff_option'          => 'page fluide, `navigation` et `extra` fixes',
	'saisie_layout_group_2lll_option'          => 'page fluide, `navigation` et `extra` fluides',
	'saisie_layout_group_2llh_option'          => 'page fluide, `navigation` et `extra` fluides sous le contenu',
	'saisie_layout_group_2llp_option'          => 'page fluide, `navigation` fluide et `extra` seule sous le contenu',
	'saisie_layout_group_2lfp_option'          => 'page fluide, `navigation` fixe et `extra` seule sous le contenu',
	'saisie_layout_group_2fff_option'          => 'page fixe, `navigation` et `extra` fixes',
	'saisie_layout_group_2ffh_option'          => 'page fixe, `navigation` et `extra` fixes sous le contenu',
	'saisie_layout_page_width_fixed_label'     => 'Largeur de la page (pixels)',
	'saisie_layout_wrapper_width_liquid_label' => 'Largeur du contenu (fraction)',
	'saisie_layout_aside_width_liquid_label'   => 'Largeur de la navigation (fraction)',
	'saisie_layout_aside_width_fixed_label'    => 'Largeur de la navigation (pixels)',
	'saisie_layout_extra_width_liquid_label'   => 'Largeur du bloc extra (fraction)',
	'saisie_layout_extra_width_fixed_label'    => 'Largeur du bloc extra (pixels)',
];
