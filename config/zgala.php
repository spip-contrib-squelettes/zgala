<?php
/**
 * Gestion des données de configuration et des valeurs par défaut des paramètres du plugin.
 *
 * @package SPIP\ZGALA\ADMINISTRATION
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_ZGALA_CONFIGURATION_CONTENEURS')) {
	define('_ZGALA_CONFIGURATION_CONTENEURS', ['layout']);
}

if (!defined('_ZGALA_DEFAUT_INDEX')) {
	define('_ZGALA_DEFAUT_INDEX', 'defaults');
}

/**
 * Fonction de création ou  de mise à jour de la configuration statique du plugin.
 *
 * @return void
**/
function zgala_configuration_ecrire() : void {
	// Configuration statique : on repart de zéro
	$config = [];

	// Remplacement des index de la config statique
	// -- les index sont toujours sur deux niveaux : celui fichier YAML et un niveau en dessous.
	include_spip('inc/yaml');
	foreach (_ZGALA_CONFIGURATION_CONTENEURS as $_conteneur) {
		$yaml = yaml_decode_file(find_in_path("config/_{$_conteneur}.yaml"));
		// Qu'il existe une config statique ou pas, on la remplace complètement
		// -- au préalable on supprimer l'index des valeurs par défaut inclus dans le fichier
		if (isset($yaml[_ZGALA_DEFAUT_INDEX])) {
			unset($yaml[_ZGALA_DEFAUT_INDEX]);
		}
		// Inclure toute la configuration statique d'un coup
		$config[$_conteneur] = $yaml;
	}

	// Mise à jour en meta
	include_spip('inc/config');
	ecrire_config('zgala_config', $config);
}

/**
 * Fonction de création ou  de mise à jour des valeurs par défaut des paramètres du plugin.
 *
 * @return void
**/
function zgala_defaut_ecrire() : void {
	// Valeurs par défaut : on repart de zéro
	$defaut = [];

	// Remplacement des index des valeurs par défaut
	include_spip('inc/yaml');
	foreach (_ZGALA_CONFIGURATION_CONTENEURS as $_conteneur) {
		$yaml = yaml_decode_file(find_in_path("config/_{$_conteneur}.yaml"));
		// Qu'il existe une liste de valeurs par défaut des paramètres ou pas, on la remplace complètement
		if (isset($yaml[_ZGALA_DEFAUT_INDEX])) {
			$defaut[$_conteneur] = $yaml[_ZGALA_DEFAUT_INDEX];
		}
	}

	// Mise à jour en meta
	include_spip('inc/config');
	ecrire_config('zgala_defaut', $defaut);
}

/**
 * Renvoie la liste des identifiants de groupe de layouts filtrée éventuellement sur le nombre de colonnes
 * desdits layouts.
 *
 * @param null|int $nb_colonnes Nombre de colonnes d'un layout (2 ou3)
 *
 * @return array Liste des id de groupe
 */
function zgala_configuration_layout_groupes(?int $nb_colonnes = null) {
	// Récupérer la configuration des structures de layouts
	include_spip('inc/config');
	$structures = lire_config('zgala_config/layout/structures', []);

	// Extraire la liste des groupes
	$groupes = array_unique(array_column($structures, 'group'));

	// Filtrer suivant le nombre de colonnes demandé ou pas
	if (null !== $nb_colonnes) {
		$groupes = array_filter($groupes, function ($groupe) use ($nb_colonnes) {
			return (int) substr($groupe, 0, 1) === $nb_colonnes;
		});
	}

	return $groupes;
}

/**
 * Renvoie la liste des identifiants de groupe de layouts filtrée éventuellement sur le nombre de colonnes
 * desdits layouts.
 *
 * @param string   $id_groupe   Identifiant du groupe de layouts
 * @param null|int $nb_colonnes Nombre de colonnes d'un layout (2 ou 3)
 *
 * @return array Tableau des paramètres avec l'indication si il sont utiles ou pas [id_groupe] = true/false
 */
function zgala_configuration_layout_parametres(string $id_groupe, ?int $nb_colonnes = null) {
	// Récupérer la configuration des paramètres de layout et de leur répertition par groupe
	include_spip('inc/config');
	$config_parametres = lire_config('zgala_config/layout/sizes', []);

	// Chercher les paramètres utiles au groupe demandé.
	// -- Si le nombre de colonnes n'est pas fourni on le détermine car cela simplifie la recherche
	if (null === $nb_colonnes) {
		$nb_colonnes = (int) substr($id_groupe, 0, 1);
	}

	$parametres = [];
	foreach ($config_parametres as $_parametre => $_groupes) {
		$parametres[$_parametre] = in_array($id_groupe, $_groupes[$nb_colonnes]);
	}

	return $parametres;
}
