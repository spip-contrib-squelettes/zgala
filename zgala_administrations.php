<?php
/**
 * Gestion de l'installation et de la mise à jour du schéma de données du plugin.
 *
 * @package SPIP\ZGALA\ADMINISTRATION
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Installation du schéma de données propre au plugin et gestion des migrations suivant
 * les évolutions du schéma.
 *
 * Le plugin crée une configuration du squelette fournie aux fichiers SCSS.
 *
 * @param string $nom_meta_base_version
 * @param string $version_cible
 *
 * @return void
 */
function zgala_upgrade($nom_meta_base_version, $version_cible) {
	// Insertion de la configuration statique dans la meta du squelette
	include_spip('config/zgala');
	$maj['create'] = [
		['zgala_configuration_ecrire'],
		['zgala_defaut_ecrire']
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Suppression de l'ensemble du schéma de données propre au plugin.
 *
 * @param string $nom_meta_base_version
 *
 * @return void
 */
function zgala_vider_tables($nom_meta_base_version) {
	// Effacer les meta du plugin
	// -- paramétrage du plugin
	effacer_meta('zgala');
	// -- configuration du plugin
	effacer_meta('zgala_config');
	// -- valeurs par défaut du plugin
	effacer_meta('zgala_defaut');
	// -- schéma du plugin
	effacer_meta($nom_meta_base_version);
}
