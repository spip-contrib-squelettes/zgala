<?php
/**
 * Ce fichier contient les cas d'utilisation des pipelines.
 *
 * @package SPIP\ZGALA\Pipelines
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Suppression des blocs head et head_js de la liste des blocs pouvant, par défaut,
 * accueillir des noisettes.
 *
 * @pipeline noizetier_blocs_defaut
 *
 * @param array $blocs Liste blocs par défaut concernés par le NoiZetier
 *
 * @return array Liste des blocs modifiés. On supprime les blocs head et head_js.
 */
function zgala_noizetier_blocs_defaut($blocs) {
	static $blocs_exclus = ['head', 'head_js'];
	if ($blocs) {
		$blocs = array_values(array_diff($blocs, $blocs_exclus));
	}

	return $blocs;
}

/**
 * Passage du paramétrage de squelette en variables SCSS.
 *
 * @pipeline scss_variables
 *
 * @param array $data Tableau des variables déjà collectées
 *
 * @return array Tableau des variables complété
 */
function zgala_scss_variables($data) {
	include_spip('inc/config');

	// Calculer les variables de paramétrage du layout :
	// -- on ne fournit que les variables non nulles car le fichier SCSS possèdent déjà des valeurs par défaut.
	// -- comme on revient systématiquement dans ce pipeline pour chaque compilation de fichier SCSS, on utilise
	//    une statique pour éviter de recalculer les variables n fois.
	static $layout = [];
	if (!$layout) {
		// Structure de chaque layout
		$structures = lire_config('zgala_config/layout/structures', []);
		if (!empty($structures)) {
			// Il faut construire la map car SCSSPHP ne le fait pas
			$layout['layout-structures'] = array_to_scssmap($structures);
		}

		// Paramétrage du layout courant
		$parametrage = lire_config('zgala/layout', []);
		if ($parametrage) {
			foreach ($parametrage as $_parametre => $_valeur) {
				if (!empty($_valeur)) {
					// Identification de la variable SCSS
					$parametre_scss = 'layout-' . str_replace('_', '-', $_parametre);
					$layout[$parametre_scss] = $_valeur;
					// Ajout d'une unité si besoin
					if ($_parametre !== 'id') {
						$layout[$parametre_scss] .= strpos($_parametre, '_fixed') !== false ? 'px' : 'fr';
					}
				}
			}
		}
	}

	// Ajouter les variables de paramétrage du layout
	if ($layout) {
		$data = array_merge($data, $layout);
	}

	return $data;
}

/**
 * Convertit un tableau PHP en une chaine représentant une map SCSS.
 *
 * @param array    $tableau            Tableau PHP
 * @param null|int $niveau_indentation Niveau d'indentation (0 par défaut)
 *
 * @return string Map SCSS au format chaine
 */
function array_to_scssmap(array $tableau, ?int $niveau_indentation = 0): string {
	// Initialisations de la map (pour l'imbrication en cours) et de l'indentation
    $map = '';
    $indentation = str_repeat('  ', $niveau_indentation);

	// Ouverture de la map
    $map .= "(\n";

    foreach ($tableau as $_cle => $_valeur) {
    	// Formater la clé selon que c'est un entier ou une chaine
        $cle_formatee = is_numeric($_cle) ? $_cle : "\"{$_cle}\"";

        if (is_array($_valeur)) {
            // Appel récursif pour les sous-tableaux
            $map .= $indentation . "  $cle_formatee: " . array_to_scssmap($_valeur, $niveau_indentation + 1);
        } else {
            // Conversion de la valeur en chaîne SCSS
            $valeur_formatee = is_numeric($_valeur) ? $_valeur : "\"{$_valeur}\"";
            $map .= $indentation . "  $cle_formatee: $valeur_formatee";
        }

        $map .= ",\n";
    }

	// Fermeture de la map
    $map .= $indentation . ")";

    return $map;
}
